{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    pre-commit-hooks,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      version = builtins.substring 0 8 self.lastModifiedDate;
    in rec {
      packages = {
        default = packages.prntserve;
        oci = packages.prntserveOci;
        prntserve = pkgs.callPackage ./nix {inherit version self;};
        prntserveOci = pkgs.dockerTools.buildImage {
          name = "prntserve";
          tag = version;
          copyToRoot = pkgs.buildEnv {
            name = "image-root";
            paths = [packages.prntserve];
            pathsToLink = ["/bin"];
          };
          config = {
            Entrypoint = ["/bin/prntserve"];
            ExposedPorts = {"3000/tcp" = {};};
          };
        };
      };
      checks = {
        pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            alejandra.enable = true;
            rustfmt.enable = true;
            prettier.enable = true;
          };
        };
      };
      devShells.default = pkgs.mkShell {
        inherit (self.checks.${system}.pre-commit-check) shellHook;
        buildInputsFrom = [packages.default];
        packages = with pkgs; [cargo];
      };
    });
}
