prntserve
=========
Store and serve your funny pictures a.k.a. screenshots.

# Abstract
prntserve is a web application that statically serves files. These files can either be added via the underlying file system or by using a HTTP PUT request on the path where the file should be displayed.

The main use-case for this application is a personal screenshot-sharing service. Though as any filetype can be shared via this method, it can also be used for other applications.

# API
prntserve uses a very simple RESTful API.

## Viewing files
`GET /<filename>` or `HEAD /<filename>`

**Parameters:**
- `filename`: name of the file (generally the path to the file, relative to document root)

**Headers:**
*None*

**Response:**
- `200 OK`: File exists
- `307 Temporary Redirect`: Effective filename is different from request (e.g. `/hello/../1.png` redirects to `/1.png`)
- `404 Not Found`: File does not exist

**Example:**
```shell
$ curl -L https://prntserve.example/hello_world.png
```

## Uploading files
`PUT /<filename>`

**Parameters:**
- `filename`: name of the file (generally the path to the file, relative to document root)

**Headers:**
- `Authorization: Bearer <token>`: token or password, which was defined as server parameter

**Response:**
- `200 OK`: File uploaded
- `307 Temporary Redirect`: Effective filename is different from request (e.g. `/hello/../1.png` redirects to `/1.png`)
- `401 Unauthorized`: no/incorrect `token`
- `409 Conflict`: Target filename is in use

**Example:**
```shell
$ curl -LT ~/screenshot.png -H "Authorization: Bearer <token>" https://prntserve.example/unixporn/epic_screenshot.png
```

## Deleting files
`DELETE /<filename>`

**Parameters:**
- `filename`: name of the file (generally the path to the file, relative to document root)

**Headers:**
- `Authorization: Bearer <token>`: token or password, which was defined as server parameter

**Response:**
- `200 OK`: File/Directory deleted
- `307 Temporary Redirect`: Effective filename is different from request (e.g. `/hello/../1.png` redirects to `/1.png`)
- `401 Unauthorized`: no/incorrect `token`
- `403 Forbidden`: Target filename can not ne deleted
- `409 Conflict`: Target filename is neither a file nor a directory

**Quirks**
- Directories need to be empty to be deleted

**Example:**
```shell
$ curl -LX DELETE -H "Authorization: Bearer <token>" https://prntserve.example/unixporn/epic_screenshot.png
```

# Clients
Currently the only known client for prntserve is [screenshot-bash](https://gitlab.com/Scrumplex/screenshot-bash), which is a very un-sophisticated, but extensible, series of scripts to create a screenshot-upload pipeline.

# Deployment

## Docker CLI
```shell
$ docker run -e PRNT_PATH=/data -e PRNT_SECRET=my_secret_token -v "/srv/prntserve-data:/data" -p 8080:8080 registry.gitlab.com/scrumplex/prntserve:master
```

## Docker Compose
```yml
version: "3"

services:
  prntserve:
    image: registry.gitlab.com/scrumplex/prntserve:master
    restart: unless-stopped
    environment:
      PRNT_PATH: /data
      PRNT_SECRET: my_secret_token
    volumes:
      - ./prntserve-data:/data
    ports:
      - 8080:8080

```

# License

This project is licensed under the GNU General Public License v3. You can read
the full license in the [LICENSE](LICENSE) file.
