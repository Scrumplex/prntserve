{
  lib,
  rustPlatform,
  self,
  version,
}:
rustPlatform.buildRustPackage {
  pname = "prntserve";

  inherit version;

  src = lib.cleanSource self;

  cargoLock = {
    lockFile = ../Cargo.lock;
  };

  meta = with lib; {
    description = "
Store and serve your funny pictures a.k.a. screenshots";
    homepage = "https://codeberg.org/Scrumplex/prntserve";
    license = licenses.gpl3Plus;
    maintainers = [maintainers.Scrumplex];
  };
}
