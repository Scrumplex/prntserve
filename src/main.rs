use std::io::Error as IoError;
use std::path::{PathBuf};
use std::sync::Arc;

use async_std::{fs::OpenOptions, fs::remove_file, fs::remove_dir, io};
use tide::{Body, Request, Response, StatusCode, Next, Middleware, Redirect};
use clap::{crate_version, crate_authors, Parser};
use std::fs::create_dir_all;
use path_clean::{clean};
use tide::http::Method;
use tide::http::headers::AUTHORIZATION;

#[derive(Parser, Debug)]
#[clap(author = crate_authors!(), version = crate_version!(), about = "Store and serve your funny pictures", long_about = None)]
struct Opts {
    #[clap(short = 'p', long, required = true, value_name = "PATH", env = "PRNT_PATH", help = "Served data directory path")]
    path: String,
    #[clap(short = 's', long, required = true, value_name = "SECRET", env = "PRNT_SECRET", help = "Secret token to protect upload endpoint")]
    secret: String,
}


fn actual_path(s: &str) -> String {
    return clean(s);
}

fn check_authorization_header(header_value: &str, token: &str) -> bool {
    header_value.trim_start_matches("Bearer ") == token
}


#[derive(Clone)]
struct PrntServeState {
    path: Arc<PathBuf>,
}

impl PrntServeState {
    fn try_new(path: String) -> Self {
        let data_path = PathBuf::from(path);
        Self {
            path: Arc::new(data_path),
        }
    }
}

struct AuthenticationMiddleware {
    methods: Vec<Method>,
    secret: String,
}

impl AuthenticationMiddleware {
    fn new(methods: Vec<Method>, secret: String) -> Self {
        AuthenticationMiddleware {
            methods,
            secret,
        }
    }
}

#[tide::utils::async_trait]
impl<PrntServeState: Clone + Send + Sync + 'static> Middleware<PrntServeState> for AuthenticationMiddleware {
    async fn handle(&self, req: Request<PrntServeState>, next: Next<'_, PrntServeState>) -> tide::Result {
        if !self.methods.contains(&req.method()) {
            return Ok(next.run(req).await);
        } else if let Some(value) = req.header(AUTHORIZATION) {
            if check_authorization_header(value.as_str(), &self.secret) {
                return Ok(next.run(req).await);
            }
        }

        Ok(Response::new(StatusCode::Unauthorized))
    }
}

#[async_std::main]
async fn main() -> Result<(), IoError> {
    let opts: Opts = Opts::parse();

    tide::log::start();
    let mut app = tide::with_state(PrntServeState::try_new(opts.path));

    app
        .with(AuthenticationMiddleware::new(vec![Method::Put, Method::Delete], opts.secret))
        .at("/*file")
        .put(|req: Request<PrntServeState>| async move {
            let filename = req.param("file")?;
            let clean_filename = actual_path(filename);
            if clean_filename != filename {
                return Ok(Redirect::temporary(clean_filename).into());
            }
            let fs_path = req.state().path.join(&clean_filename);

            if fs_path.exists() {
                return Ok(Response::new(StatusCode::Conflict));
            }

            create_dir_all(fs_path.parent().expect("Requested file does not have parent directory"))?;

            let file = OpenOptions::new()
                .create(true)
                .write(true)
                .open(&fs_path)
                .await?;

            let bytes_written = io::copy(req, file).await?;

            tide::log::info!("file written", {
                bytes: bytes_written,
                path: fs_path.canonicalize()?.to_str()
            });

            Ok(Response::new(StatusCode::Ok))
        })
        .delete(|req: Request<PrntServeState>| async move {
            let filename = req.param("file")?;
            let clean_filename = actual_path(filename);
            if clean_filename != filename {
                return Ok(Redirect::temporary(clean_filename).into());
            }
            let fs_path = req.state().path.join(clean_filename);

            if !fs_path.exists() {
                return Ok(Response::new(StatusCode::NotFound));
            }

            return if fs_path.is_file() {
                if remove_file(fs_path).await.is_ok() {
                    Ok(Response::new(StatusCode::Ok))
                } else {
                    Ok(Response::new(StatusCode::Forbidden))
                }
            } else if fs_path.is_dir() {
                if remove_dir(fs_path).await.is_ok() {
                    Ok(Response::new(StatusCode::Ok))
                } else {
                    Ok(Response::new(StatusCode::Forbidden))
                }
            } else {
                Ok(Response::new(StatusCode::Conflict))
            };
        })
        .get(|req: Request<PrntServeState>| async move {
            let filename = req.param("file")?;
            let clean_filename = actual_path(filename);
            if clean_filename != filename {
                return Ok(Redirect::temporary(clean_filename).into());
            }
            let fs_path = req.state().path.join(clean_filename);

            if let Ok(body) = Body::from_file(fs_path).await {
                Ok(body.into())
            } else {
                Ok(Response::new(StatusCode::NotFound))
            }
        });

    app.listen("0.0.0.0:8080").await?;
    Ok(())
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn actual_path_resolves_super() {
        assert_eq!(actual_path("/1/../2.png"), "/2.png");
    }

    #[test]
    fn actual_path_does_not_escape_root() {
        assert_eq!(actual_path("/../1.png"), "/1.png");
    }

    #[test]
    fn actual_path_removes_bad_slashes() {
        assert_eq!(actual_path("/1//2.png"), "/1/2.png");
    }

    #[test]
    fn actual_path_handles_cwd() {
        assert_eq!(actual_path("/1/./2.png"), "/1/2.png");
    }

    #[test]
    fn check_authorization_header_finds_good_token() {
        assert_eq!(check_authorization_header("Bearer TokenFromSouthPark", "TokenFromSouthPark"), true);
    }

    #[test]
    fn check_authorization_header_finds_bad_token() {
        assert_eq!(check_authorization_header("Bearer TokenFromSouthPark", "TokenFromTheSimpsons"), false);
    }

    #[test]
    fn check_authorization_header_finds_bad_header_value() {
        // Should return false, as Beerer is not what we want (we want bears)
        assert_eq!(check_authorization_header("Beerer TokenFromSouthPark", "TokenFromSouthPark"), false);
    }
}
